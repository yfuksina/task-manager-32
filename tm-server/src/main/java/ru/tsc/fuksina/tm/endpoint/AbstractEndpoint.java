package ru.tsc.fuksina.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.service.IServiceLocator;
import ru.tsc.fuksina.tm.dto.request.AbstractUserRequest;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;


import java.util.Optional;

public abstract class AbstractEndpoint {

    protected void check(AbstractUserRequest request, Role role) {
        check(request);
    }

    protected  void check(AbstractUserRequest request) {
        @Nullable final String userId = request.getUserId();
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
    }

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
