package ru.tsc.fuksina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.fuksina.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.fuksina.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.fuksina.tm.dto.response.TaskUnbindFromProjectResponse;

public interface IProjectTaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

}
